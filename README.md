# Recurso

El desarrollo de un Recurso Educativo Digital de un Hipertexto

## Getting Started

Esta dearrollado en Html5, Css3 y Javascript para utilizarlo solo debe dar clic en el index.html, es un desarrollo web donde solo necesitarás  un navegador y coneccion a Internet.

## Running the tests

No esta implementado aún.


## Built With

* [Moonlight](https://themewagon.com/themes/responsive-one-page-bootstrap-template/) - The web framework used


## Contributing

 Por favor para Contribuir en el desarrollo de este recurso debes dirigirte al siguiente repositorio[jean-bot](https://gitlab.com/jean-bot/recurso) Gracias por tus aportes.



## Authors

* **Jean Pierre Giovanni Arenas Ortiz** - *Initial work* - [jean-bot](https://gitlab.com/jean-bot)


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

